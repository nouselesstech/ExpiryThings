Enum CredentialUse {
    AppRegistration
    SsoIntegration
}

Enum CredentialType {
    Password
    Key
}

Enum CredentialStatus {
    Expired
    Expiring
    NotExpiring
    PendingActivation
}

Class ExpiryObject {
    [CredentialStatus]$Status
    [CredentialType]$Type
    [CredentialUse]$Use
    [String]$ApplicationId
    [String]$ObjectId
    [String]$ExpirationDate
    [String]$CredentialId

    ExpiryObject(){}
}

Function Compare-CredentialExpiration {
    # Evaluates the credential for expiration status
    param(
        $Credential,
        $App,
        $WarningDate,
        $TodayDate,
        $Type,
        $Use
    )

    # Get the dates for the current credential
    $ExpirationDate = Get-Date -Date $Credential.EndDateTime
    $StartDate = Get-Date -Date $Credential.StartDateTime

    # Establish the return object 
    $CredReturn = [ExpiryObject]::New()
    $CredReturn.ApplicationId = $App.AppId
    $CredReturn.ObjectId = $App.Id
    $CredReturn.ExpirationDate = $ExpirationDate
    $CredReturn.CredentialId = $Credential.KeyId
    $CredReturn.Type = $Type 
    $CredReturn.Use = $Use 
    
    # Run the Comparison
    $Expired = $ExpirationDate -lt $TodayDate
    $Expiring = $ExpirationDate -gt $TodayDate -and $ExpirationDate -lt $WarningDate 
    $NotExpiring = $ExpirationDate -gt $WarningDate
    $PendingActivation = $StartDate -gt $TodayDate

    if ($PendingActivation) {
        $CredReturn.Status = ([CredentialStatus]::PendingActivation)
    } elseif ($NotExpiring) {
        $CredReturn.Status = ([CredentialStatus]::NotExpiring)
    } elseif ($Expiring) {
        $CredReturn.Status = ([CredentialStatus]::Expiring)
    } elseif ($Expired) {
        $CredReturn.Status = ([CredentialStatus]::Expired)
    } else {
        throw "Could not evaluate the date status of the credential $($CredReturn.CredentialId)"    
    }

    return $CredReturn
}

Function Compare-SamlIntegrationExpiration {
    # Evaluates the SAML integration for expiration status
    param(
        $PrincipalInfo,
        $WarningDate,
        $TodayDate,
        $Type,
        $Use
    )

    # Get the dates for the current credential
    $ExpirationDate = Get-Date -Date $PrincipalInfo.PreferredTokenSigningKeyEndDateTime

    # Establish the return object 
    $CredReturn = [ExpiryObject]::New()
    $CredReturn.ApplicationId = $PrincipalInfo.AppId
    $CredReturn.ObjectId = $PrincipalInfo.Id
    $CredReturn.ExpirationDate = $ExpirationDate
    $CredReturn.CredentialId = $PrincipalInfo.PreferredTokenSigningKeyThumbprint
    $CredReturn.Type = $Type 
    $CredReturn.Use = $Use 
    
    # Run the Comparison
    $Expired = $ExpirationDate -lt $TodayDate
    $Expiring = $ExpirationDate -gt $TodayDate -and $ExpirationDate -lt $WarningDate 
    $NotExpiring = $ExpirationDate -gt $WarningDate

    if ($NotExpiring) {
        $CredReturn.Status = ([CredentialStatus]::NotExpiring)
    } elseif ($Expiring) {
        $CredReturn.Status = ([CredentialStatus]::Expiring)
    } elseif ($Expired) {
        $CredReturn.Status = ([CredentialStatus]::Expired)
    } else {
        throw "Could not evaluate the date status of the credential $($CredReturn.CredentialId)"    
    }

    return $CredReturn
}

try {
    Clear-Host
    $Return = @()

    $WarnLimit = 30
    $WarningDate = (Get-Date).AddDays($WarnLimit)
    $TodayDate = (Get-Date)

    Write-Information "Connecting to Microsoft Graph." -InformationAction Continue
    Connect-MgGraph `
        -NoWelcome `
        -Scopes Directory.Read.All

    ## Go through all the Application Registration Credentials
    Write-Information "Getting all App Registrations." -InformationAction Continue
    $Apps = Get-MgApplication

    Write-Information "Review all App Registrations." -InformationAction Continue
    For($i=0; $i -lt $Apps.count; $i++) {
        $App = $Apps[$i]
        Write-Progress `
            -Id 1 `
            -Activity "Evaluating App Registrations" `
            -Status $App.DisplayName `
            -PercentComplete ($i+1 * 100 / $Apps.count)
        
        ## Review all the password credentials
        ForEach($PwdCred in $App.PasswordCredentials) {
            $Return += Compare-CredentialExpiration `
                            -Credential $PwdCred `
                            -WarningDate $WarningDate `
                            -TodayDate $TodayDate `
                            -Use ([CredentialUse]::AppRegistration) `
                            -Type ([CredentialType]::Password) `
                            -App $App
        }

        ## Review all the key credentials
        ForEach($PwdCred in $App.KeyCredentials) {
            $Return += Compare-CredentialExpiration `
                            -Credential $PwdCred `
                            -WarningDate $WarningDate `
                            -TodayDate $TodayDate `
                            -Use ([CredentialUse]::AppRegistration) `
                            -Type ([CredentialType]::Key) `
                            -App $App
        }
    }

    Write-Progress `
        -Id 1  `
        -Activity "Evaluating App Registrations" `
        -Status "Completed" `
        -Completed

    ## Go through all the Enterprise App SSO registrations
    Write-Information "Getting all SAML integrations" -InformationAction Continue
    $SsoPrincipals = Get-MgBetaServicePrincipal -Filter "PreferredSingleSignOnMode eq 'saml'"

    Write-Information "Evaluating all SAML integrations." -InformationAction Continue
    For($i=0; $i -lt $SsoPrincipals.count; $i++) {
        $App = $SsoPrincipals[$i]
        Write-Progress `
            -Id 1 `
            -Activity "Evaluating SAML Integrations" `
            -Status $App.DisplayName `
            -PercentComplete ($i+1 * 100 / $SsoPrincipals.count)
        
        $Return += Compare-SamlIntegrationExpiration `
                        -PrincipalInfo $App `
                        -WarningDate $WarningDate `
                        -TodayDate $TodayDate `
                        -Use ([CredentialUse]::SsoIntegration) `
                        -Type ([CredentialType]::Key) 
    }
    Write-Progress `
        -Id 1  `
        -Activity "Evaluating App Registrations" `
        -Status "Completed" `
        -Completed

    Write-Information "Exporting results." -InformationAction Continue
    $Return | Export-Csv -NoTypeInformation "SecretExpiry.csv"

} catch {
    Write-Error "Unable to complete the scan. $_"

} finally {
    Disconnect-MgGraph
}