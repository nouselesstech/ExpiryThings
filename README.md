# ExpiryThings

A simple tool to pull up expriring credentials for service principals and app registrations in the environment of Microsoft 365. This is read only but requires the scope "Directory.Read.All". You can modify it to use fewer permissions, and with enough requests I will do so.

## Prequisites
- Microsoft.Graph PowerShell Module
- Microsoft.Graph.Beta PowerShell Module
- Read permissions to applications and their credentials. Global reader is a little heavy handed, but works.

## Running
1. Download/Clone the Repo.
1. Run the script ./ExpiryThings.ps1.
1. Authenticate when asked.
1. Wait for the results.
1. Open the file "SecretExpiry.csv"